class Calculator {
    constructor(previousNumTextElement, currentNumTextElement){
        this.previousNumTextElement = previousNumTextElement;
        this.currentNumTextElement = currentNumTextElement;
        this.clear();
    }

    clear(){
        this.previousNum = '';
        this.currentNum = '';
        this.operation = undefined;
    }
    appendNumber(number){
        if(number === "." && this.currentNum.includes('.')) return;
        this.currentNum = this.currentNum.toString() + number.toString(); //add more number
    }
    chooseOperation(operation){
        //when press operation => display currentNum / when twice => empty bc of current
        console.log(this.currentNum);
        if(this.currentNum === '') return;
        //calcul in previous
        if(this.previousNum !== ''){
            this.calcul();
        }
        //pass in previous
        this.operation = operation;
        this.previousNum = this.currentNum;
        this.currentNum = '';
    }
    calcul(){
        let result;
        const prev = parseFloat(this.previousNum);
        const current = parseFloat(this.currentNum);
        if(isNaN(prev) || isNaN(current)) return;
        switch (this.operation) {
            case '+':
                result = prev + current;
                break
            case '-':
                result = prev - current;
                break
            case 'x':
                result = prev * current;
                break
            case '/':
                result = prev / current;
                break
            case '%':
                result = prev % current;
                break
            default:
                return
        }
        //after press equal
        this.currentNum = result;
        this.operation = undefined;
        this.previousNum = '';
    }
    deleteOneNumber(){
        this.currentNum = this.currentNum.toString().slice(0, -1);
    }
    updateDisplay(){
        this.currentNumTextElement.innerText  = this.currentNum;
        if(this.operation != null){
            this.previousNumTextElement.innerText = `${this.previousNum} ${this.operation}`
        }else {
            this.previousNumTextElement.innerText = '';
        }
    }
}


const numberBtns = document.querySelectorAll('[data-number]');
const operationBtns = document.querySelectorAll('[data-operation]');
const clear = document.querySelector('[data-clear]');
const remove = document.querySelector('[data-remove]');
const equalBtn = document.querySelector('[data-equal]');
const previousNumTextElement = document.querySelector('.previous-num');
const currentNumTextElement = document.querySelector('.current-num');

const calculator = new Calculator(previousNumTextElement, currentNumTextElement);

numberBtns.forEach(button => {
    button.addEventListener('click', () => {
        calculator.appendNumber(button.innerText);
        calculator.updateDisplay();
    })
})

operationBtns.forEach(button => {
    button.addEventListener('click', () => {
        calculator.chooseOperation(button.innerText);
        calculator.updateDisplay();
    })
})

equalBtn.addEventListener('click', () => {
    calculator.calcul();
    calculator.updateDisplay();
})

clear.addEventListener('click', () => {
    calculator.clear();
    calculator.updateDisplay();
})

remove.addEventListener('click', () => {
    calculator.deleteOneNumber();
    calculator.updateDisplay();
})
