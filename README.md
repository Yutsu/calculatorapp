# Calculator Application
![Design homepage of the calculator application](./images/home.png)

## Description
This is a calculator application using HTML/CSS and Javascript. The design of the calculator is just like the one on the Iphone.

## Contains
Javascript class

Operator : addition, subtraction , division, modulo and multiplication

Result of the operation

Erase one or all numbers.

## Technologies Used
Html

Css

Javascript

## Installation
git clone https://gitlab.com/Yutsu/calculatorapp.git

## Start Server
Live server from Vscode Extension and click "Go live"

## Ressources

**Olia danilevich**

https://www.pexels.com/fr-fr/photo/crayon-trombones-ecole-maquette-5088008/
